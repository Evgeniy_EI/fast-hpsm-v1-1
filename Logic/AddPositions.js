// Константы хранения элемента позиций:

// Константы "Краткого описания"
var ELEM_CHORTDESCR_CONNECTION = "Подключение";
var ELEM_CHORTDESCR_NULL_SDO = "Обнуление СДО";
var ELEM_CHORTDESCR_DIRECTORY = "Справочник";
var ELEM_CHORTDESCR_CONSULTATION = "Консультация";
var ELEM_CHORTDESCR_DEL_AUTO_RESOLUTION = "Удаление автоматического направления";
var ELEM_CHORTDESCR_ADMIN_NOMENCLTR = "Администрирование номенклатуры";

// Обращаемся к элементам фрейма
var iframe;
var iframe_contentWindow;
var iframe_document;

var IFRAME_CSS_SLCTR = 'iframe[title="Новое обращение"]';               //iframe
var TEMPLATES_CSS_SLCTR = 'button[aria-label="Применить шаблон"]';   // Обращаемся к кнопке "Применить шаблон"

// Константы "Описания"
var ELEM_OPIS_CONNECTION = "Курсы пройдены, проверено";
var ELEM_OPIS_NULL_SDO = "Обнуление заблокированного пункта в курсах СДО.";
var ELEM_OPIS_DIRECTORY = "Добавление в справочник ЭДО нового адресата.";
var ELEM_OPIS_CONSULTATION = "Проведена консультация по ";
var ELEM_OPIS_DEL_AUTO_RESOLUTION = "Удаление автоматического направления";
var ELEM_OPIS_ADMIN_NOMENCLTR = "Обнуление индекса номенклатуры";

// Константы "Решения"
var ELEM_DECISION_CONNECTION = "";
var ELEM_DECISION_NULL_SDO = "Добрый день!\nЗаблокированный пункт в курсах СДО обнулен.\nСпасибо за обращение.";
var ELEM_DECISION_DIRECTORY = "Добрый день!\nНовый адресат добавлен в справочник ЭДО.\nСпасибо за обращение.";
var ELEM_DECISION_CONSULTATION = "Добрый день!\nПроведена консультация по !!!ДОПИСАТЬ!!!  \nСпасибо за обращение.";
var ELEM_DECISION_DEL_AUTO_RESOLUTION = "Добрый день!\nАвтоматическое направление в документе удалено.\nСпасибо за обращение.";
var ELEM_DECISION_ADMIN_NOMENCLTR = "Добрый день!\nБыл обнулен индекс номенклатуры.\nСпасибо за обращение.";

// Константы айдишников элементов
var DOC_ELEM_ID = 'X61';          // Краткое описание
var DESCR_ELEM_ID = 'X63';        // Описание
var DECISION_ELEM_ID = 'X121';    // Решение 

// Активация кнопки расширения

// Проверка открытого фрейма
if(document.querySelector('[title="Новое обращение"]')) {
    ini_iframe();
    show_exention_panel();
}

function ini_iframe() {
    iframe = document.querySelector(IFRAME_CSS_SLCTR);
    iframe_contentWindow = iframe.contentWindow;
    iframe_document = iframe.contentWindow.document;
}


// Активация окна расширения
var TOP_HPSM_PANEL = document.getElementById("cwc_header");

function show_exention_panel() {
let show_Activ_button_top = document.createElement("a");

show_Activ_button_top.setAttribute("onclick", "");
show_Activ_button_top.id = "act_btn";
show_Activ_button_top.innerHTML = "Применить шаблон:";
show_Activ_button_top.className= "Activ_button_top";

TOP_HPSM_PANEL.append(show_Activ_button_top);

let Show_block_select_positions = document.createElement("div");
let button_select_position_connection = document.createElement("a");
let button_select_position_null_sdo = document.createElement("a");
let button_select_position_consultation = document.createElement("a");
let button_select_position_auto_resolution = document.createElement("a");
let button_select_position_admin_nomecltr = document.createElement("a");
let button_select_position_directory = document.createElement("a");

Show_block_select_positions.className = "style_Show_block_select_positions";  // присваиваем класс фону кнопок выбора позиций
button_select_position_connection.className = "style_button_select_position_connection";      // присваиваем класс кнопке выбора позиции
button_select_position_null_sdo.className = "style_button_select_position_null_sdo";
button_select_position_consultation.className = "style_button_select_position_consultation";
button_select_position_auto_resolution.className = "style_button_select_position_auto_resolution";
button_select_position_admin_nomecltr.className = "style_button_select_position_admin_nomecltr";
button_select_position_directory.className = "style_button_select_position_directory";

Show_block_select_positions.id = "id_block_select";             // айди блока кнопок позиций
button_select_position_connection.id = "id_bt_connection";
button_select_position_null_sdo.id = "id_bt_null_sdo";
button_select_position_consultation.id = "id_bt_consultation";
button_select_position_auto_resolution.id = "id_bt_resolution";
button_select_position_admin_nomecltr.id = "id_bt_nomecltr";
button_select_position_directory.id = "id_bt_directory";

button_select_position_connection.setAttribute("onclick", "press_connect()");     // Присваиваем атрибуты
button_select_position_null_sdo.setAttribute("onclick", "");
button_select_position_directory.setAttribute("onclick", "");
button_select_position_consultation.setAttribute("onclick", "");
button_select_position_auto_resolution.setAttribute("onclick", "");
button_select_position_admin_nomecltr.setAttribute("onclick", "");

button_select_position_connection.innerHTML = "Подключение";
button_select_position_null_sdo.innerHTML = "Обнуление СДО";
button_select_position_directory.innerHTML = "Справочник";
button_select_position_consultation.innerHTML = "Консультация";
button_select_position_auto_resolution.innerHTML = "Авто.Направление";
button_select_position_admin_nomecltr.innerHTML = "Номенклатура";

TOP_HPSM_PANEL.append(Show_block_select_positions);
Show_block_select_positions.append(button_select_position_connection, button_select_position_null_sdo, 
button_select_position_consultation, button_select_position_auto_resolution, button_select_position_admin_nomecltr, 
button_select_position_directory);

// Показываем / скрываем панель расширения
    let show_act_btn = document.getElementById("act_btn");
    show_act_btn.onclick = function () {
        let show_close_panel = document.getElementById("id_block_select");
        let act_btn_dark = document.getElementById("act_btn");

        show_close_panel.classList.toggle("style_button_block_select");
        act_btn_dark.classList.toggle("Activ_button_top_notpress");
    }
}